# -*- coding7: utf-8 -*-
"""
Note to self: this has been superceded by `njh.py` from the repo
`git@bitbucket.org:jezhill/bin`, which gets softlinked to `~/.ipython/njh.py`
(whence it will get auto-imported by
`~/.ipython/profile_default/startup/import_magics.py`) as part of `link_all.sh`.

IPython "magic" shortcuts, including:

    * edit
    * search
    * l
    * reimport
    * desk
    * undesk
    * addpath

For any one given session, you can activate these magics just by performing::

	import snapmagic

To avoid having to do the import every time, use the ``%bootstrap_shortcuts``
shortcut once: this will create an ``.ipy`` file in the ``startup`` directory
of your IPython profile, which will perform ``import snapmagic`` automatically
at the start of every future session.

@author: jezhill
"""

try: get_ipython; __OLDER_IPYTHON__ = False; __IPYTHON__ = get_ipython()
except NameError: __OLDER_IPYTHON__ = True; get_ipython = lambda: __IPYTHON__
try: __IPYTHON__
except NameError: pass
else:

	import os, sys

	if sys.version >= '3': unicode = str;  basestring = ( bytes, str ) # bytes is already defined, unicode is not
	else: bytes = str # unicode is already defined, bytes is not
	def IfStringThenRawString( x ):    return x.encode( 'utf-8' ) if isinstance( x, unicode ) else x
	def IfStringThenNormalString( x ): return x.decode( 'utf-8' ) if str is not bytes and isinstance( x, bytes ) else x
	__SPYDER__ = 'SPYDER_ARGS' in os.environ and 'SPYDER_PARENT_DIR' in os.environ

	def get_ipython_workspace(): return __IPYTHON__.shell.user_ns if __OLDER_IPYTHON__ else get_ipython().user_ns

	exec( 'import os,sys', get_ipython_workspace() ) # just in case this is being run via import rather than execute

	################################################################################
	################################################################################

	try:
		from IPython.core.magic import magics_class, Magics, line_magic
	except ImportError:
		oldmagic = True
		magics_class = lambda x: x
		Magics = object
		def line_magic(f):
			name = f.__name__
			if __OLDER_IPYTHON__:
				if not name.startswith('magic_'): name = 'magic_' + name
				def wrapped(*pargs, **kwargs): return f(None, *pargs,**kwargs)
				if hasattr(f, '__doc__'): wrapped.__doc__ = f.__doc__
				setattr(__IPYTHON__, name, wrapped)
			else:
				if name.startswith('magic_'): name = name[6:]
				get_ipython().define_magic(name, f)
			return f
	else:
		oldmagic = False

	############################################################################

	def getmagic(name):
		if __OLDER_IPYTHON__:
			if not name.startswith('magic_'): name = 'magic_' + name
			f = getattr(__IPYTHON__, name)
			return f
			#def wrapped(*pargs, **kwargs): return f(None, *pargs,**kwargs)
			#if hasattr(f, '__doc__'): wrapped.__doc__ = f.__doc__
			#return wrapped
		else:
			if name.startswith('magic_'): name = name[6:]
			return get_ipython().find_magic(name)

	############################################################################

	def bang(arg, stdin=None, shell=True, verbose=False):
		"Because os.system() doesn't print stdout/stderr to Jupyter notebooks"
		windows = sys.platform.lower().startswith('win')
		# Now, if shell is False, we have to split the arg into a list---otherwise the entirety of the string will be assumed to be the name of the binary.
		# By contrast, if shell is True, we HAVE to pass it as all one string---in a massive violation of the principle of least surprise, subsequent list items are then passed as flags to the shell executable, not to the targeted executable.
		# Note: windows seems to need shell=True otherwise it doesn't find even basic things like ``dir`` - perhaps it does not search the %PATH% ?
		# On other platforms it might be best to pass shell=False due to security issues, but note that you lose things like ~ and * expansion
		if not shell:
			if windows: arg = arg.replace('\\', '\\\\') # otherwise shlex.split will decode/eat backslashes that might be important as file separators
			import shlex; arg = shlex.split(arg) # shlex.split copes with quoted substrings that may contain whitespace
		if verbose: print(repr(arg))
		import subprocess
		sp = subprocess.Popen(arg, shell=shell, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdout, stderr = sp.communicate(stdin)
		if stderr: sys.stderr.write( IfStringThenNormalString( stderr ) )
		if stdout: sys.stdout.write( IfStringThenNormalString( stdout ) )
		return sp.returncode


	################################################################################
	################################################################################

	# @magics_class     # NB: Python 2.5 does not support class decorators so we'll be doing it by hand after class definition
	class MyMagics(Magics):

		if sys.platform.lower().startswith('win'):
			if hasattr(__IPYTHON__, 'rc'): __IPYTHON__.rc.editor = 'scite'
			else: __IPYTHON__.editor = 'scite'

		############################################################################

		dbstop = None
		if not dbstop:
			try: from IPython.Debugger import Tracer; dbstop = Tracer() # IPython v 0.10 and earlier
			except: pass
		if not dbstop:
			try: from IPython.core.debugger import Tracer; dbstop = Tracer() # IPython v 0.11 and later
			except: pass
		if not dbstop:
			try: from IPython.core import debugger; dbstop = debugger.Pdb().set_trace  # Some other f***ing version of IWishYoudStopMovingTheGoalposts
			except: pass

		sys.dbstop = __IPYTHON__.dbstop = dbstop
		# because calling sys.dbstop()  is easier than having to remember and type
		# "from IPython.Debugger import Tracer; Tracer()()" just to invoke the debugger
		# (and *much* easier than having to remember the version-specific variants of that)
		# Note however that you can also run in debug mode and set breakpoints with:
		#   %run -d -b LINENUMBER file.py
		#   %run -d -b OTHERFILENAME:LINENUMBER file.py

		############################################################################
		@line_magic
		def edit(self, arg):
			"""\
A more Matlab-like replacement for IPython's default editing behaviour:
search the path for possible matches; edit them in the background; don't
execute the result (NB: the latter can also be achieved in IPython with
``%edit -x``).  Set your editor with the line ``editor WHATEVER`` in
ipythonrc, or by setting ``__IPYTHON__.rc.editor = 'WHATEVER'`` in older
versions.

Examples::

	edit site.py          #  edits existing file on path with .py extension     $BLAH/Lib/site.py
	edit ipythonrc        #  edits existing file on path without .py extension  $HOME/.ipython/ipythonrc
	edit site             #  adds .py extension and edits existing file on path $BLAH/Lib/site.py

	edit IPython/iplib.py #  edits existing file in subdirectory on path $BLAH/IPython/iplib.py
	edit IPython/iplib    #  edits existing file in subdirectory on path $BLAH/IPython/iplib.py
	edit IPython.iplib    #  edits existing file in package on path      $BLAH/IPython/iplib.py
	edit xml              #  edits __init__.py file in existing package on path $BLAH/xml/__init__.py

	edit foo.py           #  edits new file with .py extension     ./foo.py
	edit foo.txt          #  edits new file with non-.py extension ./foo.txt
	edit foo              #  adds .py extension and edits new file ./foo.py

	edit IPython/foo.py   #  edits new file in existing subdirectory on path $BLAH/IPython/foo.py
	edit IPython/foo      #  edits new file in existing subdirectory on path $BLAH/IPython/foo.py
	edit IPython.foo      #  edits new file in existing subdirectory on path $BLAH/IPython/foo.py

Most Windows or Unixoid editors will behave well if you give them the name of a not-yet-
existing file to edit.  On OSX, however, if you're using an editor script that redirects
to ``open -a Some.app file.txt``, this will fail if file.txt doesn't exist. For this reason,
you can use ``edit -t ....`` where the ``-t`` stands for "touch":  the file will be created if it
doesn't already exist, before being opened in the editor.  (Typos will then sometimes mean
that you're left with unwanted 0-byte files hanging around, needing to be deleted by hand.)
			"""###

			# NB: one (unlikely) case is not handled:  a new file with a non-.py extension
			#     in an existing subdirectory on the path, e.g. edit IPython/foo.txt

			windows = sys.platform.lower().startswith('win')
			if __SPYDER__ and windows:
				editor = os.path.join( os.path.split( sys.executable )[ 0 ], 'Scripts', 'spyder.exe' )
				execute = bang
			else:
				if hasattr(__IPYTHON__, 'rc'): editor = __IPYTHON__.rc.editor
				else: editor = __IPYTHON__.editor
				if windows: editor = 'start ' + editor
				execute = os.system
			def edit(x, mode='existing'):
				if not x: return execute( editor ) or True
				x = os.path.abspath(x)
				if mode == 'existing' and not os.path.isfile(x): return False
				if os.path.isdir(x): return sys.stderr.write( 'cannot edit directory %s\n' % x ) and False
				if mode == '-t' and not os.path.isfile(x): open(x, 'a').close()
				execute(editor + ' "' + x.strip('"') + '"')
				return True

			if edit(arg): return
			# that allows for spaces in the path, e.g.  edit C:\Documents and Settings\me\Desktop\blah.py
			# but if the whole thing isn't an existing file, we'll assume a space-delimited list of filenames.
			arg = arg.strip()
			
			#try: arg = eval(arg, get_ipython_workspace()).__file__
			#except: pass
			
			ip = get_ipython_workspace()
			if arg.startswith('(') and arg.endswith(')'):
				arg = eval('['+arg[1:-1]+']', ip)
			else:
				if windows: arg = arg.replace('\\', '\\\\') # otherwise shlex.split will decode/eat backslashes that might be important as file separators
				import shlex; arg = shlex.split(arg) # shlex.split copes with quoted substrings
			creation_policy = arg.pop(0) if (arg and arg[0] == '-t') else 'auto'

			for original in arg:
				try: obj = eval( original, ip )
				except: obj = None
				else:
					try: edit( obj.__file__ )
					except: pass
					else: continue
				try: edit( original.__file__ )
				except: pass
				else: continue
				try: original = original.__module__
				except: pass
				original = os.path.expanduser( original )
				stem = original
				if stem.lower().endswith('.py'): stem = stem[:-3]
				original_withpy = stem + '.py'
				path_withoutpy = os.path.join(*stem.split('.'))
				path_withpy = path_withoutpy + '.py'
				potential_location = ''
				for p in sys.path:
					if   edit(os.path.join(p, original)): break
					elif edit(os.path.join(p, original_withpy)): break
					elif edit(os.path.join(p, path_withpy)): break
					elif edit(os.path.join(p, path_withoutpy)): break
					elif stem == original and edit(os.path.join(p, path_withoutpy, '__init__.py')): break
					elif potential_location == '':
						pp = os.path.join(p, path_withoutpy)
						dd = os.path.realpath(os.path.dirname(pp))
						d  = os.path.realpath(p)
						if len(dd) > len(d) and os.path.isdir(dd): potential_location = pp
				else: # not found anywhere else, so edit as new
					if potential_location == '': potential_location = original
					fn, xtn = os.path.splitext(potential_location)
					if xtn == '': xtn = '.py'
					edit(fn + xtn, creation_policy)

		############################################################################
		@line_magic
		def addpath(self, d=None):
			"""\
Make absolute paths out of the input(s) and append them to ``sys.path`` if
they are not already there.  Supply a space-delimited list of arguments, or
one argument enclosed in quotes (which may, in that case, contain a space).
	"""###
			if d == None: dd = (os.getcwd(),)
			else:
				d = d.replace('\\ ', ' ').strip()
				if (d.startswith('"') and d.endswith('"')) or (d.startswith("'") and d.endswith("'")):
					dd = (d[1:-1],)
				else:
					dd = d.split()
			for d in dd:
				d = os.path.realpath(d)
				if os.path.isdir(d):
					if not d in sys.path: sys.path.append(d)
				else:
					print('no such directory "%s"' % d)

		############################################################################
		@line_magic
		def desk(self, subdir=''):
			"""\
cd to the Desktop, but keep a global record of where we were (stored in
``__IPYTHON__.undeskdir``) so that we can snap back with ``%undesk``
			"""###
			if sys.platform.lower().startswith('win'):
				homedrive = os.environ.get('HOMEDRIVE')
				homepath = os.environ.get('HOMEPATH')
				userprofile = os.environ.get('USERPROFILE')
				if userprofile == None:
					d = os.path.join(homedrive, homepath, 'Desktop')
				else:
					d = os.path.join(userprofile, 'Desktop')
			else:
				d = os.path.join(os.environ.get('HOME'), 'Desktop')
			if subdir != '':
				return os.path.realpath(os.path.join(d, subdir))
			here = os.path.realpath(os.curdir)
			if os.path.realpath(d) == here:
				print("already at desktop")
			else:
				__IPYTHON__.undeskdir = here
				print('changing directory from ' + here)
				print('                     to ' + d)
				print('type %undesk to change back')
				os.chdir(d)

		############################################################################
		@line_magic
		def undesk(self, arg):
			"""\
Undo a previous call to ``%desk``
			"""###
			d = getattr(__IPYTHON__, 'undeskdir', None)
			if d == None:
				print('no global record of previous call to %desk')
			else:
				print('changing directory from ' + os.path.realpath(os.curdir))
				print('                back to ' + d)
				os.chdir(d)


		############################################################################
		@line_magic
		def search(self, dd):
			"""\
Usage::

	search  TERM
	search  TERM   TOP_LEVEL_DIR
	search  TERM   TOP_LEVEL_DIR   FILE_PATTERN

Searches case-insensitively for ``TERM`` through all files matching
``FILE_PATTERN`` (default: ``'*'``) in the directory tree starting at
``TOP_LEVEL_DIR`` (default: ``'.'``) and prints the filename, line
number and line content for any matches.

Because, on Windows, native file content searches are not reliable and
IPython is going to be the best command-line you have.

Also, for convenience, ``search --filename TERM TOP_LEVEL_DIR`` does the
simpler job of matching file names rather than file content. Example::

	search --filename .py .

			"""###
			import os, sys, fnmatch, re, getopt
			args = [ ''.join( matches ) for matches in re.findall( '|'.join( [ r'"([^"]*)"', r"'([^']*)'", r'(\S+)' ] ), dd ) ]
			opts, args = getopt.getopt( args, '', [ 'filename' ] )
			opts = dict( opts )
			if not 1 <= len( args ) <= 3: sys.stderr.write( 'arguments: SEARCH_STRING [TOP_DIR] [FILE_PATTERN]\n' ); sys.stderr.flush(); return
			search_string, root_dir, file_pattern = ( args + [ '', '', '' ] )[ :3 ]
			sys.stdout.write( '\n' )
			sys.stdout.flush()
			if not root_dir: root_dir = '.'
			matches = []
			if not os.path.isdir( root_dir ):
				sys.stderr.write( str( root_dir ) + ' is not a directory\n' )
				sys.stderr.flush()
				return

			search_string = str( search_string ).lower()
			try: search_bytes = search_string.encode( 'utf-8' )
			except: search_bytes = search_string
			limit = 200
			for root, dirnames, filenames in os.walk( root_dir ):
				if file_pattern: filenames = fnmatch.filter( filenames, file_pattern )
				for filename in filenames:
					file_path = os.path.join ( root, filename )
					if '--filename' in opts:
						if search_string in str( os.path.split( filename )[ 1 ] ).lower():
							sys.stdout.write( '%s\n' % ( file_path.replace( '\\', '/' ) ) )
							sys.stdout.flush()
							matches.append( file_path )
						continue
					matches.append( file_path )
					try: file_handle = open( file_path, 'rb' )
					except IOError:
						sys.stderr.write( '/!\\   failed to open %s\n\n' % file_path )
						sys.stderr.flush()
					for line_number, line_bytes in enumerate( file_handle ):
						try: line_string = line_bytes.decode( 'utf-8' )
						except: line_string = line_bytes; hit = search_bytes in line_bytes; printable = False
						else: hit = search_string in line_string.lower(); printable = True
						if hit:
							if printable: sys.stdout.write( '%s:%d:\n    %s\n\n' % ( file_path.replace( '\\', '/' ), line_number + 1, line_string.strip()[ : limit ] ) )
							else: sys.stdout.write( '%s:%d  matches (no preview available)\n\n' % ( file_path, line_number + 1 ) )
							sys.stdout.flush()
			if len( matches ) == 0:
				sys.stderr.write( '/!\\   no files matched the input pattern\n' )
				sys.stderr.flush()
			else:
				sys.stdout.write( '\n' )
		############################################################################
		@line_magic
		def reimport(self, dd):
			"""\
The syntax::

    %reimport foo, bar.*

is a shortcut for the following::

    import foo; foo = reload(foo)
    import bar; bar = reload(bar); from bar import *

			"""###
			tempws = {}
			reloader = None
			if not reloader:
				try: reloader = reload
				except NameError: pass
			if not reloader:
				try: from importlib import reload as reloader
				except: pass
			if not reloader:
				try: from imp import reload as reloader
				except: pass
			tempws[ '__reload' ] = reloader
			for d in dd.replace( ',', ' ' ).split( ' ' ):
				if len( d ):
					bare = d.endswith( '.*' )
					if bare: d = d[ :-2 ]
					exec( 'import xx; xx = __reload(xx)'.replace( 'xx', d ), tempws )
					if bare: exec( 'from xx import *'.replace( 'xx', d ), tempws )
			tempws.pop( '__reload' )
			get_ipython_workspace().update( tempws )

	############################################################################
		@line_magic
		def l(self, dd='.'):
			"""\
Quickly print a listing of the current directory contents.
			"""###
			if dd in [ None, '' ]: dd = '.'
			dd = dd.replace( '\\ ', ' ' ).strip().strip( '"\'' )
			if sys.platform.lower().startswith( 'win' ): cmd = 'dir "%s"' % dd.replace( '/', '\\' )
			else: cmd = 'ls -AFlh %s' % dd
			bang( cmd, shell=True )

	############################################################################
	
		@line_magic
		def kill_pyc( self, dd='.' ):
			"""\
Recursively remove .pyc files from one or more directory trees.
			"""
			import shlex
			files = [ os.path.join( parent, file ) for d in shlex.split( dd ) for parent, dirs, files in os.walk( os.path.realpath( d ) ) for file in files if file.lower().endswith( '.pyc' ) ]
			for file in files: print( 'removing %s' % file ); os.remove( file )
			
	############################################################################
		@line_magic
		def bootstrap_shortcuts(self, prefix='00_'):
			"""\
``import snapmagic`` allows you access to these useful shortcuts (edit,
search, desk, addpath, ...).  The ``bootstrap_shortcuts`` shortcut writes
the import command into an .ipy file in the default ipython profile directory
of your home directory, so that the shortcuts will be loaded automatically
whenever you start IPython (versions 0.13 and later) in future.
			"""###
			code = """\
try: import snapmagic
except: print ("Failed to import snapmagic")

try: # Also execute order 66: "jedi" tab-completion is a huge PITA, so let's fall back on IPython-native tab-completion:
	%config IPCompleter.use_jedi = False
except: pass
"""
			import os, sys
			if __OLDER_IPYTHON__: sys.stderr.write( "\nFAILED: this bootstrapper only works for newer versions of IPython.\nYou'll have to add the following lines manually to ipy_user_conf.py\nin the _ipython or .ipython subdirectory of your home directory:\n\n%s\n" % code ); return
			if sys.platform.lower().startswith( 'win' ): home = os.environ[ 'USERPROFILE' ]
			else: home = os.environ[ 'HOME' ]
			startup_dir = os.path.join( home, '.ipython', 'profile_default', 'startup' )
			startup_file = os.path.join( startup_dir, prefix + 'import_snapmagic.ipy' )  # cannot be a .py file, must be an .ipy - the latter seem to be executed later, when ipython is running and its instance can be accessed with get_ipython()
			if not os.path.isdir( startup_dir ): os.makedirs( startup_dir )
			open( startup_file, 'wt' ).write( code )
			sys.stderr.write( '\nSUCCESS: the file\n   %s\nhas been created containing the following lines:\n\n%s\n' % ( startup_file, code ) )

	################################################################################
		@line_magic
		def bootsnap_addpath( self, *pargs ):
			"""
Usage::

	%bootsnap_addpath XYZ

Is a shortcut for::

	import bootsnap; bootsnap.AddPath( 'XYZ' )

			"""
			import bootsnap
			bootsnap.AddPath( *pargs )
			
	################################################################################
		@line_magic
		def plt( self, *pargs ):
			ws = get_ipython_workspace()
			exec( 'import matplotlib.pyplot as plt, numpy, numpy as np; plt.ion()', ws )
			
	################################################################################
		@line_magic
		def bootsnap_import( self, x ):
			"""
Usage::

	%bootsnap_import PARENT_DIR/MODULE
	%bootsnap_import PARENT_DIR/MODULE/SYMBOLS

Examples::

	%bootsnap_import snaptools/snap.Plotting
	%bootsnap_import snaptools/snap.Plotting/*
	%bootsnap_import snaptools/snap.Plotting/plot,imagesc

Performs the following actions:
	(1) if necessary, adds 'snaptools' to the system path using bootsnap.AddPath()
	(2) imports snap.Plotting
	(3) imports particular symbols from snap.Plotting if specified
			"""
			import re, bootsnap
			args = re.sub( r'[\s/,]+', '/', x ).split( '/' )
			parent = args.pop( 0 )
			if parent: bootsnap.AddPath( parent )
			module = args.pop( 0 )
			ws = get_ipython_workspace()
			exec( 'import ' + module, ws )
			args = [ arg for arg in args if arg ]
			if args: exec( 'from ' + module + ' import ' + ','.join( args ), ws )

	################################################################################
	################################################################################

	MyMagics = magics_class( MyMagics )
	if not oldmagic:
		get_ipython().register_magics( MyMagics )

	#del MyMagics # Remove class definition from namespace again: even in oldmagic, side-effects of the decorators will have already done the work
	#if __name__ == '__main__':
	#	del getmagic, get_ipython_workspace, oldmagic
	#	del line_magic, magics_class, Magics

