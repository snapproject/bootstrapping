:: Optionally install a copy of this SciTE as a text editor under "Program Files"
:: or "Program Files (x86)". Then, optionally, add the resulting SciTE directory
:: to the path. Finally, also optionally, set it as the editor for .bat files.

:: Note that the changes to environment variables won't be initially recognized. You
:: need to restart, or go to Control Panel->System->Advanced->Environment variables
:: and click OK, or use some binary that "broadcasts a window message", as explained
:: in http://stackoverflow.com/questions/24500881 and
::    http://stackoverflow.com/questions/20653028


:: Detect admin rights a la   https://stackoverflow.com/a/11995662
:: If they are absent, escalate a la   https://superuser.com/a/1250352
:: (NB: the latter requires PowerShell, which may not be available on old systems)
@net session >NUL 2>NUL || ( powershell start -wait -verb runas "%~f0" && GOTO :eof )
:: (NB2: a more-elaborate more-robust version of this exists - see scraps\self_elevate.bat or https://stackoverflow.com/a/54701990/ )


@set "dest=%PROGRAMFILES%"
@if "%PROGRAMFILES(x86)%" == "" goto end32
:start32
@set "dest=%PROGRAMFILES(x86)%"
:end32



@set "dest=%dest%\SciTE"
@if exist "%dest%" goto endmkdir
:startmkdir
@mkdir "%dest%"
:endmkdir
@copy /-Y "%~dp0\SciTE-editor\scite.exe"              "%dest%\"
@copy /-Y "%~dp0\SciTE-editor\SciTEGlobal.properties" "%dest%\"


::@set "EnvKey=HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment"
::@FOR /f "usebackq tokens=2*" %%a IN (`REG QUERY "%EnvKey%" /v Path`) do @set "OldPath=%%~b"
::@echo Adding %dest% to Path environment variable:
:: Approach 1:
::@SETX /M Path "%OldPath%;%dest%
:: Approach 2:
::@reg add "%EnvKey%" /v Path /t REG_EXPAND_SZ /d "%OldPath%;%dest%
@call "%~dp0AddToWindowsPath" END "%dest%

@echo Setting %dest%\scite.exe as default editor for .bat files:
@reg add "HKEY_CLASSES_ROOT\batfile\shell\edit\command" /ve /d "%dest%\scite.exe %%1"

@echo Setting %dest%\scite.exe as default editor for .cmd files:
@reg add "HKEY_CLASSES_ROOT\cmdfile\shell\edit\command" /ve /d "%dest%\scite.exe %%1"

@set /p ANS="Should the command `edit` invoke scite? (y/n): "
@if /I "%ANS%"=="y" copy /-Y "%~dp0\SciTE-editor\edit.cmd" "%dest%\"

@pause
