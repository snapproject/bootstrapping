#!/usr/bin/env bash # 2>NUL
:<<@GOTO:EOF
: ###########################
: ### Windows ###############

python "%~dp0\bootsnap.py"

: ###########################
@GOTO:EOF
{ tr -d \\r|bash;}<<:EOF
: ###########################
: ### POSIX  ################

python "`dirname $0`/bootsnap.py"

: ###########################
:EOF
