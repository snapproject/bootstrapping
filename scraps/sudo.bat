:: Adapted from https://stackoverflow.com/a/54701990/
:: See self_elevate.bat instead for an example of how to
:: self-elevate to admin privileges within a batch script
@set args=%*
@if defined args set args=%args:^=^^%
@if defined args set args=%args:<=^<%
@if defined args set args=%args:>=^>%
@if defined args set args=%args:&=^&%
@if defined args set args=%args:|=^|%
@if defined args set "args=%args:"=\"\"%"
@powershell -NoProfile -ExecutionPolicy Bypass -Command ^
  " Start-Process -Wait -Verb RunAs -FilePath cmd -ArgumentList \"/c \"\" cd /d \"\"%CD%\"\" ^&^& %args% \"\" \" "
