:: Self-elevation-to-admin-privileges section
:: adapted from https://stackoverflow.com/a/54701990/
@net session >NUL 2>NUL && goto :elevated
@setlocal
@set args=%*
@if defined args set args=%args:^=^^%
@if defined args set args=%args:<=^<%
@if defined args set args=%args:>=^>%
@if defined args set args=%args:&=^&%
@if defined args set args=%args:|=^|%
@if defined args set "args=%args:"=\"\"%"
@powershell -NoProfile -ExecutionPolicy Bypass -Command ^
  " Start-Process -Wait -Verb RunAs -FilePath cmd -ArgumentList \"/c \"\" cd /d \"\"%CD%\"\" ^&^& \"\"%~f0\"\" %args% \"\" \" "
@exit /b
:elevated
:: After the user confirms escalation in the UAC consent dialog,
:: a new console window will be open, run the following code, and
:: close again (change /c to /k in the powershell command line above
:: to keep the admin prompt open after executing the code)




@echo off
echo %~nx0 is running as admin in %CD%
echo First argument is "%~1"
echo Second argument is "%~2"
pause
