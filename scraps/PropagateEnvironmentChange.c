/*

When you change environment variables such as %Path%, the change is not propagated
to running processes (including windows explorer, which will start new processes...)
until you either reboot or manually go into environment variables and click OK.

This is a workaround. Compile it with:

call "%VS120COMNTOOLS%\..\..\VC\vcvarsall.bat" && cl.exe /MT PropagateEnvironmentChange.c /link user32.lib && erase PropagateEnvironmentChange.obj

See
	https://stackoverflow.com/q/24500881
	https://stackoverflow.com/a/20656609

NB you could probably do this in PowerShell too - see https://mnaoumov.wordpress.com/2012/07/24/powershell-add-directory-to-environment-path-variable/


*/
#include "windows.h"

void main( void )
{
	SendMessageTimeout(
		HWND_BROADCAST,
		WM_SETTINGCHANGE,
		(WPARAM) NULL,
		(LPARAM) "Environment",
		SMTO_NORMAL,
		1000,
		NULL
	);
}
