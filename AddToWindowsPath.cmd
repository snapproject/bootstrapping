@SET "Position=%~1
@SET "TargetDir=%~2
@SET Ok1=0
@SET Ok2=0

@IF /I "%Position%" == "end"   @set Ok1=1
@IF /I "%Position%" == "start" @set Ok1=1
@IF /I "%TargetDir%" NEQ ""    @set Ok2=1

@IF "%Ok1%%Ok2%" NEQ "11" (
@ECHO Usage:       %0   start ^| end   TARGETDIR
@GOTO :eof
)

@SET "OldDir=%CD%
@CD /D "%TargetDir%
@SET "TargetDir=%CD%
@CD /D "%OldDir%
:: %TargetDir% is now absolutized, without trailing slash

:: First let's do it for the current environment - use %Path%, which combines the user-level and system-level Path variables
@ECHO ;%Path%; | FIND /C /I ";%TargetDir%;" >NUL && @GOTO :SkipCurrentEnvironment
@ECHO ;%Path%; | FIND /C /I ";%TargetDir%\;" >NUL && @GOTO :SkipCurrentEnvironment
@SET "Path=%TargetDir%;%Path%
:SkipCurrentEnvironment

:: Now let's do it system-wide
@SETLOCAL enableDelayedExpansion
@SET "EnvKey=HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment"
@FOR /F "usebackq tokens=2*" %%a IN (`REG QUERY "%EnvKey%" /V Path`) DO @SET "OldPath=%%~b"
@ECHO ;%OldPath%; | FIND /C /I ";%TargetDir%;" >NUL && @GOTO :PathAlreadyDone
@ECHO ;%OldPath%; | FIND /C /I ";%TargetDir%\;" >NUL && @GOTO :PathAlreadyDone

@ECHO.
@ECHO Adding %TargetDir% to the Path environment variable...

:: Detect admin rights a la   https://stackoverflow.com/a/11995662
:: If they are absent, escalate a la   https://superuser.com/a/1250352
:: (NB: the latter requires PowerShell, which may not be available on old systems)
@net session >NUL 2>NUL
@IF %ERRORLEVEL% NEQ 0 (
@powershell start -wait -verb runas "%~dpfx0" "%Position%=%TargetDir%"
@GOTO :PathDone
)

:: Here's one way, which propagates the environment change itself (relies on SETX which may not be available in very old systems):
@if /I %Position%==end   @SETX /M Path "%OldPath%;%TargetDir%
@if /I %Position%==start @SETX /M Path "%TargetDir%;%OldPath%
@PAUSE

:: Here's another way (which, incidentally, also asks the user for confirmation):
::@if /I %Position%==end   @REG add "%EnvKey%" /v Path /t REG_EXPAND_SZ /d "%OldPath%;%TargetDir%
::@if /I %Position%==start @REG add "%EnvKey%" /v Path /t REG_EXPAND_SZ /d "%TargetDir%;%OldPath%
::@PropagateEnvironmentChange
:: The best way to propagate the environment change is to run PropagateEnvironmentChange.exe which would need to be
:: available in this directory.  In its absence, the ghetto way below is to launch SystemPropertiesAdvanced, and rely
:: on the user to click "Environment Variables", "OK", "OK" - and we would prefer to do that only if the Path change
:: has succeeded.  To detect that:  we can't seem to get the result of REG via %ERRORLEVEL%, and can't use && or ||
:: on the line above because of the hanging quote (and NB: don't mess with the hanging quote). Or maybe REG just
:: pretends all is fine and returns 0, even when the user cancels. Either way, the way forward would be to read
:: the registry again and see if the desired effect has been achieved:
::@FOR /F "usebackq tokens=2*" %%a IN (`REG QUERY "%EnvKey%" /V Path`) DO @SET "NewPath=%%~b"
::@ECHO ;%NewPath%; | FIND /C /I ";%TargetDir%;" >NUL && @SystemPropertiesAdvanced

@GOTO :PathDone

:PathAlreadyDone
@ECHO.
@ECHO %TargetDir% is already on the operating-system Path
@GOTO :PathDone

:PathDone
@ECHO.
@ENDLOCAL
