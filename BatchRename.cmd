""" " 2>NUL
@echo off
::cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::

set "CUSTOMPYTHONHOME=%PYTHONHOME_DEFAULT%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set "PYTHONHOME=%CUSTOMPYTHONHOME%
set "PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%~f0" %*

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
" """

import os
import re
import sys
import glob
import shutil

def BatchRename( pattern, match, replace, preview=False ):
	files = [ file for file in sorted( glob.glob( pattern ) ) if os.path.isfile( file ) ]
	if not files: print( 'no files found matching %s' % pattern ); return
	for src in files:
		src = os.path.realpath( src )
		parentDir, src = os.path.split( src )
		dst = re.sub( match, replace, src )
		src = os.path.join( parentDir, src )
		dst = os.path.join( parentDir, dst )
		#print( '' )
		if dst == src:
			print( 'no change to %s' % src )
		elif os.path.isfile( dst ):
			print( '*** !!! file already exists: %s' % dst )
		else:
			verb = 'would move' if preview else 'moving'
			blank = ' ' * len( verb )
			print( '%s from: %s' % ( verb, src ) )
			print( '%s   to: %s' % ( blank, dst ) )
			if not preview: shutil.move( src, dst )
if __name__ == '__main__':
	args = sys.argv[ 1: ]
	if len( args ) not in [ 3, 4 ]:
		sys.stderr.write("""
usage:   {execname} PATTERN MATCH REPLACE

example: {execname} *.wav  Nromal Normal
""".format( execname=os.path.basename( sys.argv[ 0 ] ) ) )
		sys.exit( 1 )
	BatchRename( *args )