@echo off

@setlocal
@set "OLDDIR=%CD%
@cd /D "%~dp0\.."
@set "SNAPROOT=%CD%
@if exist "%SNAPROOT%\Anaconda" (
	set CUSTOMPYTHONHOME=%SNAPROOT%\Anaconda
) else (
	set CUSTOMPYTHONHOME=%SNAPROOT%\anaconda3
)
@cd /D "%OLDDIR%"

@set "DEACTIVATE=%CUSTOMPYTHONHOME%\Scripts\Deactivate.bat
@if not exist "%DEACTIVATE%" goto :SkipDeactivate
@call "%DEACTIVATE%"
:SkipDeactivate

@set PYTHONHOME=%CUSTOMPYTHONHOME%
@set PATH=%PYTHONHOME%;%PATH%

@set "SCRIPT=%~1
@set "PARAMS="
:KeepCollectingParameters
@if @%1==@ goto :FinishedCollectingParameters
@shift
@set "PARAMS=%PARAMS% %1"
@goto :KeepCollectingParameters
:FinishedCollectingParameters

:: snap.cmd on its own, without a script target, just starts IPython:
if @%SCRIPT%==@ %PYTHONHOME%\Scripts\IPython & goto :eof

:: Determine absolute or relative
@python -c "import os,sys;sys.exit(1-os.path.isabs(os.environ['SCRIPT']))" || set "SCRIPT=%SNAPROOT%\%SCRIPT%

@python "%SCRIPT%" %PARAMS%
::@python "%SNAPROOT%\boottest\blah.py" %*
@endlocal
