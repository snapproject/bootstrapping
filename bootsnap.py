#!/usr/bin/env python
"""
Typical usage:

	from bootsnap import AddPath

	AddPath( 'XYZ' ) # resolves ../XYZ relative to the location of
	                 # the bootsnap module itself, and adds the result
	                 # to sys.path if not already there.

	import X, Y, Z   # or whatever modules / packages are to be found
	                 # in the XYZ toolbox path


OK, but how do you bootstrap the bootstrapper - i.e., how do you make
`from bootsnap import ...` possible in the first place?

One thing you can do is `%run bootsnap.py` from the IPython console
or do `python bootsnap.py` from your system console.  That will call
bootsnap.Main() which will (a) add bootsnap's parent directory to
`sys.path` for the current session and (b) also create a file called
"bootsnap.pth" in the "site-packages" directory: this will make
bootsnap findable lastingly in future sessions. Therefore, you only
have to do that once (or at least, once for each of your Python
distros / environments).

There is also a wrapper `bootsnap.cmd` which will run this file in
the standard SNAP Python distribution (determined via `snap.cmd`).
(If run as Administrator, it will also add its parent directory
to the operating-system path.)

@author: jezhill
"""
BEFORE = dir()

__all__ = [
	'Find',
	'AddPath',
	'Update',
	'Require',
	'WhereAmI',
]

import os
import sys
import site
import shlex
import inspect
import warnings
import distutils
import traceback
import subprocess

	
try: import setuptools
except ImportError: setuptools = None
class SetupShield( object ):
	def __setup( self, **kwargs ):
		self.kwargs = kwargs
	def __enter__( self ):
		self.__olddir = os.getcwd()
		self.__distutils_core_setup, distutils.core.setup = distutils.core.setup, self.__setup
		if setuptools: self.__setuptools_setup, setuptools.setup = setuptools.setup, self.__setup
		return self
	def __exit__( self, *pursued_by_bear ):
		distutils.core.setup = self.__distutils_core_setup
		if setuptools: setuptools.setup = self.__setuptools_setup
		os.chdir( self.__olddir )
	def Run( self, directory, setupfile='setup.py' ):
		self.filename = setupfile = os.path.realpath( os.path.join( directory, setupfile ) )
		self.kwargs = {}
		self.error = None
		if setupfile and os.path.isfile( setupfile ):
			os.chdir( directory )
			code = open( setupfile, 'rt' ).read()
			ns = dict( globals() )
			ns[ 'setup' ] = self.__setup
			try: exec( code, ns, ns )
			except SyntaxError as err: error_class, detail, line_number = err.__class__.__name__, '', err.lineno
			except: cls, instance, tb = sys.exc_info(); error_class, detail, line_number = cls.__name__, ': %s' % instance, traceback.extract_tb( tb )[ -1 ][ 1 ]
			else: return
			self.error = Warn( 'aborted parsing of %s due to %s at line %d%s' % ( setupfile, error_class, line_number, detail ) )
			
def PackageDirs( root ):
	paths = [ root ]
	with SetupShield() as setup: setup.Run( root )
	package_dir = setup.kwargs.get( 'package_dir', None )
	if package_dir:
		def valid( path ):
			path = os.path.realpath( os.path.join( root, path ) )
			if os.path.isdir( path ): return [ path ]
			else: Warn( 'ignoring non-existent directory %r in `package_dir` argument to `setup()` in %s' % ( path, setup.filename ) ); return []
		paths = [ path for value in package_dir.values() for path in valid( value ) ]
		if not paths: paths = [ root ]
	return paths

def Warn( msg ):
	sys.stderr.write( '%s\n' % msg ); return msg
	#formatwarning_orig = warnings.formatwarning
	#warnings.formatwarning = lambda message, category, filename, lineno, line=None, **k: formatwarning_orig( message, category, filename, lineno, line='' )
	#warnings.warn( msg )
	#warnings.formatwarning = formatwarning_orig
	#return msg

BOOTSTRAPPED_PATHS = set()

def WhereAmI( forwardSlashes=False, up=0 ):
	here = inspect.currentframe()
	caller = inspect.getouterframes( here )[ 1 ][ 0 ]
	caller = inspect.getfile( caller )
	if not os.path.isfile( caller ): return None # e.g. '<string>' if called from exec('WhereAmI()') or <stdin> if called direct from python interpreter, or <ipython-input-*> if called from IPython console
	myDir = os.path.dirname( os.path.realpath( caller ) )
	if up: myDir = os.path.realpath( os.path.join( myDir, *( [ '..' ] * up ) ) )
	if forwardSlashes: myDir = myDir.replace( '\\', '/' )
	return myDir
	
def Resolve( defaultRoot, *path_pieces ):
	path = os.path.join( *path_pieces )
	if not os.path.isabs( path ): path = os.path.join( defaultRoot, path )
	return os.path.realpath( path )

def FindLocal( *path_pieces ):
	return Resolve( WhereAmI( up=0 ), *path_pieces )

def Find( *path_pieces ):
	return Resolve( WhereAmI( up=1 ), *path_pieces )
		
def AddPath( *args ):
	location = 1
	args = list( args )
	if   args and isinstance( args[  0 ], ( int, type( None ) ) ): location = args.pop(  0 )
	elif args and isinstance( args[ -1 ], ( int, type( None ) ) ): location = args.pop( -1 )
	paths = [ path for arg in args for path in PackageDirs( Find( arg ) ) ]
	for path in paths:
		if not os.path.isdir( path ): raise ValueError( '%r is not a directory' % path )
		if path not in sys.path: sys.path.insert( len( sys.path ) if location is None else location, path )
		BOOTSTRAPPED_PATHS.add( path )
	return paths

def Command( *cmd_pieces ):
	cmd = [ piece for arg in cmd_pieces for piece in shlex.split( arg ) ]
	return lambda *path: subprocess.call( cmd, shell=False, cwd=Find( *path ) )
	
def hg( *cmd_pieces ):
	if cmd_pieces[ 0 ].split()[ 0 ].lower() != 'hg': cmd_pieces = ( 'hg', ) + cmd_pieces
	return Command( *cmd_pieces )
	
def Update( *paths ):
	paths = [ path for x in paths for path in ( BOOTSTRAPPED_PATHS if x == '*' else ( [ x ] if isinstance( x, str ) else x ) ) ] 
	for path in paths:
		path = Find( path )
		if not os.path.isdir( os.path.join( path, '.hg' ) ):
			Message( '%r is not an hg repository' % path )
			continue
		Command( 'hg pull --update' )( path )

class Authentication( object ):
	def __init__( self ):
		self.usernames = {}
		self.passwords = {}
	def __enter__( self ):
		return self
	def __exit__( self, etype, einst, tb ):
		self.passwords.clear()
	def Require( self, path, url ):
		raise RuntimeError( 'TODO' )
		# try to get without password, get and store password so that it only has to be typed once for multiple Requires...?
		# ...or maybe store a specially made private key (for snaplab, so read-only permissions on everything) in this bootsnap repo, and use it here?

def Message( txt ):
	sys.stderr.write( str( txt ) + '\n' )
	try: sys.stderr.flush()
	except: pass

def Require( path, url=None ):
	path = Find( path )
	if os.path.isdir( path ): return AddPath( path )
	if not url: raise ValueError( 'failed to find required toolbox %s' % path )
	Message( 'Could not find required toolbox %s\nCloning from %s' % ( path, url ) )
	if subprocess.call( [ 'hg', 'clone', url, path ] ) != 0 or not os.path.isdir( path ):
		ValueError( 'failed to download required toolbox %s' % path )
	return AddPath( path )

def CreatePthFile( path, filename='bootsnap' ):
	if not filename: filename = path
	filename = os.path.splitext( os.path.basename( filename ) )[ 0 ] + '.pth'
	sitedirs = [ d for d in site.getsitepackages() if os.path.isdir( d ) ]
	filename = os.path.join( sitedirs[ -1 ], filename )
	Message( 'Writing %s' % ( filename ) )
	with open( filename, 'wt' ) as f: f.write( path + '\n' )
	
def Clean( namespace ): namespace and [ namespace.pop( x , None ) for x in set( namespace ) - set( BEFORE ) ]
	
def Main( path=None ):
	"""
	This function is called when this file is %run from an IPython console, or
	executed from Spyder or a non-interactive python interpreter.
	
	It adds the directory that containing this module (`bootsnap`) to `sys.path`
	if it is not already there (or if it is there but only temporarily due to
	`execfile` or `%run` from an interactive interpreter).
	
	After running, it should be possible to `import bootsnap` or `from bootsnap
	import AddPath` to allow you to proceed in adding sibling toolbox paths.
	
	This function also creates a ".pth" file inside the site-packages directory
	of the current Python environment, so that you should be able to do
	`import bootsnap` directly in future sessions without running this file/calling
	`Main()` first.
	"""
	if path: path = os.path.realpath( path )
	else: path = WhereAmI() # works if this file is run with %run or execfile, but execfile is dropped in Python 3 in favour of exec(open(...).read()) in which case this will return None	
	if not path:
		try: __file__
		except: pass
		else: path = os.path.realpath( os.path.join( __file__, '..' ) )
	if not path:
		raise( 'This file cannot be run using exec(open(...).read()). You will have to add it to the path manually, import Main from it, and call Main()' )
	location = None
	if '' in sys.path: # if so, we're likely in an interactive interpreter - in that case having `path` in position 0 doesn't count because it's probably only there temporarily due to execfile or %run
		if path not in sys.path[ 1: ]: location = sys.path.index( '' ) + 1
	elif path not in sys.path:
		location = 0
	Message( 'Python\'s sys.path is as follows:\n\n  ' + '\n  '.join( sys.path ) + '\n' )	
	if path in sys.path[ 1: ]: Message( "%s is already on Python\'s sys.path" % path )
	else: CreatePthFile( path )
	Message( '' )
	if location is not None: sys.path.insert( location, path )

if __name__ == '__main__':
	Main( *getattr( sys, 'argv', [] )[ 1:2 ] )
	Clean( locals() )
else:
	del BEFORE

# yes, it's this many lines because flexible bootstrapping truly is annoying
