:<<@EXIT/B
:: First, adds the directory to the system path.
:: 
:: Second, runs bootsnap.py, which creates a .pth file to
:: this location in site-packages. This .cmd script does this
:: inside the canonical SNAP Python distro (determined in
:: snap.cmd) - if you want to set up your own custom distro
:: or environment, just run `python bootsnap.py` yourself.
::
:: The final step is one that this `bootsnap.py` does not do
:: for you and you may want to do by hand: open ipython,
:: and type::
::
::     import snapmagic
::     %bootstrap_shortcuts


@SET "Here=%~dp0
:: This will have a trailing slash, so let's remove it:
@SET "Here=%Here:~0,-1%

:: (1) Add it to the system path (escalating to Admin privileges
::     if necessary) and broadcast that environment change:
@CALL "%~dp0AddToWindowsPath" START "%Here%

:: (2) Run bootsnap.py to create .pth file
@CALL "%Here%\snap.cmd" "%Here%\bootsnap.py"
@pause

:::::: End of Windows code
@EXIT/B
# Start of posix code
# TODO: manage the operating-system path
python "`dirname $0`/bootsnap.py" #
