' This launches any Windows console process (from a .bat, .cmd,
' or console application like python.exe) with the console window
' suppressed.
' Simply prefix the path to this file to the command line you would
' otherwise use. Or, if .vbs files are not executable directly for
' whatever reason, prefix C:\Windows\System32\wscript.exe followed
' by the path to this file.
' 
' https://superuser.com/a/699910

If WScript.Arguments.Count >= 1 Then
	ReDim arr( WScript.Arguments.Count - 1 )
	For i = 0 To WScript.Arguments.Count - 1
		Arg = WScript.Arguments( i )
		If InStr( Arg, " " ) > 0 Then Arg = """" & Arg & """"
		arr( i ) = Arg
	Next
	RunCmd = Join( arr )
	CreateObject( "Wscript.Shell" ).Run RunCmd, 0, True
End If
